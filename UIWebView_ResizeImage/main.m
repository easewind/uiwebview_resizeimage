//
//  main.m
//  UIWebView_ResizeImage
//
//  Created by easewind on 14-5-14.
//  Copyright (c) 2014年 easewind.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
