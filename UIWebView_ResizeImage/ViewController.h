//
//  ViewController.h
//  UIWebView_ResizeImage
//
//  Created by easewind on 14-5-14.
//  Copyright (c) 2014年 easewind.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIWebViewDelegate>

@end
